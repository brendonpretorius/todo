require 'test_helper'

class ApiTodoTest < ActionDispatch::IntegrationTest
  test "get /api/todos.json" do
    get "/api/todos.json"
    assert_response :success
    assert body == Todo.all.to_json
    todos = JSON.parse(response.body)
    assert todos.size == 4 # because there are three fixtures (see screencasts.yml)
    assert todos.any? { |s| s["title"] == todos(:one).title }
  end

  test "get /api/todos/:id" do
    todo = todos(:one)
    get "/api/todos/#{todo.id}.json"
    assert_response :success
    assert body == todo.to_json
    assert JSON.parse(response.body)["title"] == todo.title
  end
  
  
end
