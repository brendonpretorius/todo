require 'test_helper'

class TodoTest < ActiveSupport::TestCase
  setup do
    @todo1 = todos(:one)
    @todo2 = todos(:two)
    @todo3 = todos(:three)
    @todo4 = todos(:four)
   
  end
  
  test "should assert validity" do
    assert @todo1.valid?
  end
  
  test "should assert that required data is required" do
    @todo5 = Todo.new
    assert !@todo5.valid?
  end
  
  
end
