class CreateTodos < ActiveRecord::Migration
  def change
    create_table :todos do |t|
      t.boolean :done, default: :false
      t.string :title, null: false
      t.string :description

      t.timestamps null: false
    end
  end
end
