angular.
	module('core.todo').
	factory('ToDo', ['$resource',
		function($resource){
			return $resource('http://glacial-falls-31685.herokuapp.com/api/todos.json', {}, {
        	getAll: {
          		method: 'GET',
          		isArray: true
        	},
        	
        	deleteAllDone: {
        		method: 'DELETE',
        		url: 'http://glacial-falls-31685.herokuapp.com/api/todos_done.json'
        	},
        	        	
        	deleteTodo: {
        		method: 'DELETE',
        		params: {id: 'id'},
        		url: 'http://glacial-falls-31685.herokuapp.com/api/todos/:id.json'
        	},
        	
        	deleteAll: {
        		method: 'DELETE',
        		url: 'http://glacial-falls-31685.herokuapp.com/api/todos_all.json'
        	},
        	
        	createTodo: {
        		method: 'POST',
        		
        		transformResponse: function(data, headers){
                	data = JSON.parse(data);
                	return data;
            	}        		
        	},
        	
        	updateTodo: {
        		method: 'PUT',
        		params: {id: '@id'},
        		url: 'http://glacial-falls-31685.herokuapp.com/api/todos/:id.json',
        		transformResponse: function(data, headers){
                	data = JSON.parse(data);
                	return data;
            	}        		
        	}       	
        	
      	 });
		}	
	]);
