angular
	.module('todoList').
	component('listOfTodo', {
		templateUrl: '/templates/todos-list.template.html',
		controller: ['ToDo', '$scope', function ListToDosController(ToDo, $scope) {
			var self = this;
			
      		self.todos = ToDo.getAll();
    		self.order = '-updated_at';
    		self.notEdit = true;
    		self.todoToEdit;
    		self.createNewFlag;
    		    		
    		self.findIndex = function findIndex(todoId)
    		{
    			for(i=0; i < self.todos.length; i++)
    			{
    				if(self.todos[i].id === todoId)
    				{
    					return i;
    				}
    			}
    		};
    	    		
    		self.deleteTodo = function deleteTodo(todoId)
    		{
    			self.todos.splice(self.findIndex(todoId), 1);
    			ToDo.deleteTodo({id: todoId});    			
    		};
    		
    		self.toggleEdit = function toggleEdit(todoId)
    		{
    			self.notEdit = !self.notEdit;
    			self.todoToEdit = self.todos[self.findIndex(todoId)];		
    		};
    		
    		self.deleteAll = function deleteAll()
    		{
    			self.todos = [];
    			ToDo.deleteAll();
    		};
    		
    		self.deleteDone = function deleteDone()
    		{    			
    			for(i = self.todos.length-1; i >= 0; i--)
    			{
    				if(self.todos[i].done === true)
    				{
    					self.todos.splice(i, 1);   					   					
    				}
    			}
    			ToDo.deleteAllDone();     			   			
    		};
    		
    		self.createNew = function createNew()
    		{
    			self.createNewFlag = true;
    		};
    		
    		self.cancelCreate = function cancelCreate(newTodo)
    		{
    			$scope.newTodo={};
    			$scope.newForm.$setPristine();
    			$scope.newForm.$setUntouched(); 
    			self.createNewFlag = false;
    		};
    		
    		self.saveTodo = function saveTodo(newTodo)
    		{
    			newTodo.done = false;
    			newTodo = ToDo.createTodo({done: newTodo.done, title: newTodo.title, description: newTodo.description});
    			self.todos.push(newTodo);
    			self.cancelCreate();
    			  				
    		};
    		
    		self.updateTodoForm = function updateTodoForm(todoToEditParam)
    		{
    			self.updateTodo(todoToEditParam);
    			self.notEdit = !self.notEdit;
    			$scope.editForm.$setPristine();
    			$scope.editForm.$setUntouched(); 
    		};
    		
    		self.updateTodo = function updateTodo(todoToEditParam)
    		{
    			self.todos[self.findIndex(todoToEditParam.id)]= ToDo.updateTodo({id: todoToEditParam.id, done: todoToEditParam.done, 
    			title: todoToEditParam.title, description: todoToEditParam.description});		
    		};
    		
    		
    	}],
    	controllerAs: 'todosList' 
	});