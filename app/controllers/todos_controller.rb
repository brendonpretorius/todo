class TodosController < ApplicationController

skip_before_filter :verify_authenticity_token

  def update
    @todo = Todo.find(params[:id])
    if @todo.update(todo_params)
      render json: @todo, status: :ok
    else
      render json: @todo.errors, status: :unprocessable_entity
    end
  end
  
  def create
     
      @todo = Todo.new(todo_params)
        if !@todo.save
          render json: @todo.errors, status: :unprocessable_entity
        else
          render json: @todo, status: :ok
        end      
  end
  
  def new
    @todo = Todo.new
  end
  
  def index
    render json: Todo.all
  end
  
  def show  
    render nothing: true and return if !Todo.exists?(params[:id]) 
    render json: Todo.find(params[:id])
  end
  
  def destroy_all_done
    Todo.where(:done => true).destroy_all
     render json: {status: :ok}     
  end
  
  def destroy
    @todo = Todo.find(params[:id])
      if !@todo.destroy
        render json: @todo.errors, status: :unprocessable_entity
      else
        render json: {status: :ok}
      end
  end
  
  def destroy_all
    Todo.destroy_all
    render json: {status: :ok}
  end
  
  def new
    @todo = Todo.new
  end
  

  def toggle_done
    toggle(:done)
  end
  
   def todo_params
    params.require(:todo).permit(:done, :title, :description)
  end
end
